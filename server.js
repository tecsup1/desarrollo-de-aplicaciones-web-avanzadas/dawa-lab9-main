const express = require("express");

const mongoose = require("mongoose");

const app = express();

const bodyParser = require("body-parser");

const port = process.env.PORT || 3000;

require("./src/config/config");

// create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: false }));

// create application/json parser
app.use(bodyParser.json());

// archivos estaticos como bootstrap:
app.use(express.static(__dirname + "/public"));
// motor de plantillas:
app.set("view engine", "pug");
app.set('views', './src/views'); // indicando donde está la carpeta view
// instanciando paquete de express-validator:
const { check, validationResult } = require("express-validator");

mongoose.connect(
    process.env.URLDB,
    {useNewUrlParser:true, useCreateIndex: true, useUnifiedTopology: true},
    //"mongodb://localhost:27017/lab07DB", 
    (err, res) => {
    if (err) throw err;
    console.log("Base de datos Online");
});


require('./src/routes/usuario')(app);


app.listen(port, () => console.log(`Escuchando puerto ${port}`));

