const express = require("express");

const bcrypt = require("bcrypt");

const Usuario = require("../models/usuario");

const app = express();

// instanciando paquete de express-validator:
const { check, validationResult } = require("express-validator");

module.exports = function (app) {
    app.post(
        "/usuario",
        [
            check("nombre")
                .isLength({ min: 3 })
                .withMessage(
                    "Su nombre completo debe tener más de 3 caracteres en POST."
                ),
            check("email")
                .isEmail()
                .withMessage("Introduzca un email válido en POST."),
            check("password")
                .isLength({ min: 3 })
                .withMessage(
                    "Su contraseña debe tener al menos 3 caracteres en POST"
                ),
        ],
        function (req, res) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.render("index", {
                    errors: errors.array(),
                });
            } else {
                let body = req.body;
                let usuario = new Usuario({
                    nombre: body.nombre,
                    email: body.email,
                    password: bcrypt.hashSync(body.password, 10),
                    role: body.role,
                    //role: 'USER_ROLE'
                });

                usuario.save((err, usuarioDB) => {
                    if (err) {
                        return res.status(400).json({
                            ok: false,
                            err,
                        });
                    }
                    usuarioDB.password = null;
                    Usuario.find({}).exec((err, usuarios) => {
                        res.render("index", {
                            usuario: usuarioDB,
                            usuarios: usuarios,
                        });
                    });
                });
            }
        }
    );

    app.get("/usuario", function (req, res) {
        Usuario.find({}).exec((err, usuarios) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err,
                });
            }
            res.render("index", {
                usuarios: usuarios,
            });
        });
    });

    app.post(
        "/usuarioUpdate",
        [
            check("id")
                .isLength({ min: 20 })
                .withMessage("Ingrese un ID correcto en UPDATE"),
            check("nombre")
                .isLength({ min: 3 })
                .withMessage(
                    "Su nombre completo debe tener más de 3 caracteres en UPDATE"
                ),
            check("email")
                .isEmail()
                .withMessage("Introduzca un email válido en UPDATE"),
            check("password")
                .isLength({ min: 3 })
                .withMessage(
                    "Su contraseña debe tener al menos 3 caracteres en UPDATE"
                ),
        ],
        function (req, res) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                
                Usuario.find({}).exec((err, usuarios) => {
                    res.render("index", {
                        errors: errors.array(),
                        usuarios: usuarios,
                    });
                });


            } else {
                // let id = req.params.id;
                let id = req.body.id;
                let body = req.body;
                Usuario.findByIdAndUpdate(
                    body.id.trim(),
                    body,
                    { new: true },
                    (err, usuarioDB) => {
                        if (err) {
                            return res.status(400).json({
                                ok: false,
                                err,
                            });
                        }
                        Usuario.find({}).exec((err, usuarios) => {
                            res.render("index", {
                                usuarios: usuarios,
                            });
                        });
                    }
                );
            }
        }
    );

    app.post("/usuarioDelete", function (req, res) {
        let id = req.params.id;
        let body = req.body;

        Usuario.findByIdAndDelete(body.id, (err, usuarioBorrado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err,
                });

                
            }
            Usuario.find({}).exec((err, usuarios) => {
                res.render("index", {
                    usuarios: usuarios,
                });
            });
        });
    });
};
/**/
// });
